import json

from mongoengine import *


class _Product(EmbeddedDocument):
    title = StringField(required=True)
    price = FloatField(required=True)
    stock = IntField(required=True)
    sku = StringField(required=True)
    extra_data = DynamicField()

class _Store(Document):
    title = StringField(max_length=300, required=True)
    description = StringField(max_length=3000, required=True)
    products = ListField(EmbeddedDocumentField(_Product), required=True)

class BuyError(Exception):
   """Base class for store related exceptions"""
   pass


class Store():
    def __init__(self, store):
        self._store = store

    def dump(self):
        json_store = self._store.to_json()
        store = json.loads(json_store)
        del store["_id"]
        return store

    @property
    def store_id(self):
        return str(self._store.id)

    def save(self):
        return self._store.save()

    def buy(self, sku):
        bought = False
        found = False
        for i, product in enumerate(self._store.products):
            if product['sku'] == sku:
                found = True
            if self._store.products[i].stock > 0:
                self._store.products[i].stock = self._store.products[i].stock - 1
                bought = True
        self._store.save()
        if not found: 
            raise BuyError("Count not find product")
        if not bought:
            raise BuyError("Item Out of Stock")


class StoreNotFoundError(Exception):
   """Base class for store related exceptions"""
   pass

def get_store(store_id):
    stores = _Store.objects(id=store_id)
    if len(stores) < 1:
        raise StoreNotFoundError("Store is missing please create a new one")
    return Store(stores[0])

class InvalidRawDataToCreateStoreError(Exception):
   """Base class for store related exceptions"""
   pass

def generate_store(raw_data):
    # validate that the store has at least one product
    try:
        if len(raw_data['products']) == 0:
            raise InvalidRawDataToCreateStoreError(f"Not Enough Products")
        products = []
        for product in raw_data['products']:
            _product = _Product(
                title=product['title'],
                price=product['price'],
                stock=product['stock'],
                sku=product['sku'],
            )
            if 'extra_data' in product:
                _product.extra_data = product['extra_data']
            products.append(_product)

        return Store(_Store(
            title=raw_data['title'],
            description=raw_data['description'],
            products=products
        ))
    except (ValidationError) as e:
        raise InvalidRawDataToCreateStoreError(str(e))
    except (KeyError) as e:
        raise InvalidRawDataToCreateStoreError("Missing key {}".format(str(e)))

