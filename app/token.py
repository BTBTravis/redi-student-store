import os

from itsdangerous import URLSafeSerializer

class InvalidTokenError(Exception):
   """Base class for store related exceptions"""
   pass

class Token_Manager:
    def __init__(self):
        self._serializer = URLSafeSerializer(os.environ['APP_SECRET_KEY'], "auth")

    def decode_token(self, token):
        try:
            return self._serializer.loads(token)
        except:
            raise InvalidTokenError()
    
    def encode_token(self, data):
        return self._serializer.dumps(data)