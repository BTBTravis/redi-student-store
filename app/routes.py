import os
from functools import wraps
from flask import Flask, session, redirect, url_for, request
from markupsafe import escape

from app import app
from app.store import get_store, StoreNotFoundError, generate_store, InvalidRawDataToCreateStoreError, BuyError
from app.token import Token_Manager, InvalidTokenError
from app.utils import pprint

token_manager = Token_Manager()

@app.route('/store/<token>', methods=['GET'])
def whole_store(token):
    try:
        token_data = token_manager.decode_token(token)
        store = get_store(token_data['store_id'])
        return store.dump()
    except KeyError:
        return "correct data not in token", 400
    except InvalidTokenError:
        return "Invlaid Token", 400
    except StoreNotFoundError:
        return "Can't find store from that token", 404


@app.route('/store', methods=['POST'])
def create_store():
    raw_data = request.get_json()
    try:
        store = generate_store(raw_data)
        store.save()
        new_token = token_manager.encode_token({
            'store_id': store.store_id
        })
        return {
            'token': new_token
        }
    except (InvalidRawDataToCreateStoreError) as e:
        return "Problem validating creating your new store: {}".format(str(e)), 400


@app.route('/buy/<sku>', methods=['POST'])
def buy(sku):
    if 'token' not in request.args:
        return "Missing token param", 400
    token = request.args['token']
    try:
        token_data = token_manager.decode_token(token)
        store = get_store(token_data['store_id'])
        store.buy(sku)
        return {
            'success': True
        }
    except KeyError:
        return "correct data not in token", 400
    except InvalidTokenError:
        return "Invlaid Token", 400
    except StoreNotFoundError:
        return "Can't find store from that token", 404
    except BuyError as e:
        return "Problem buying the product {}".format(str(e)), 400
