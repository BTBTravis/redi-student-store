import os
from flask import Flask
from flask_mongoengine import MongoEngine
from flask_cors import CORS


app = Flask(__name__)

mongo_user = os.environ['MONGO_USER']
mongo_pw = os.environ['MONGO_PW']
mongo_host = os.environ.get('MONGO_HOST', 'localhost')

app.config['MONGODB_SETTINGS'] = {
    'username': mongo_user,
    'password': mongo_pw,
    'host': mongo_host,
    'db': "studentStore"
}
app.debug = True

db = MongoEngine()
db.init_app(app)
CORS(app)

app_secret_key = os.environ['APP_SECRET_KEY']
app.secret_key = bytes(app_secret_key, 'utf-8')

from app import routes